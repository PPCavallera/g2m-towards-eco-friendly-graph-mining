# G2M Towards eco-friendly Graph Mining

## Abstract

  Graph Mining is a technique widely used to extract significant information from highly interconnected data. The algorithms used, although powerful, are often very complex and require intensive parallelization to reduce execution time. However, due to the current climate crisis, it is necessary to consider the environmental impact of the Graph Mining process, an aspect that has been largely ignored until now. Our work aims to establish an optimal compromise between reducing execution time and minimizing energy consumption. Firstly, we measure the environmental impact resulting from the Graph Mining process. Then, we propose a solution allowing users to choose the best configuration to achieve a balance between efficiency and eco-responsibility, based on their specific priorities between these two aspects.

